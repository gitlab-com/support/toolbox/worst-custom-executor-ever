#!/bin/bash

echo "'run_exec' executable running"
echo "Running '$2' sub-stage"
echo "Provided with script argument: '$1'"

# Execute the script provided
/bin/bash $1

# Handle script exit code
if [ $? -ne 0 ]; then
    # Exit using the variable, to make the build as failure in GitLab
    # CI.
    exit "$BUILD_FAILURE_EXIT_CODE"
fi
