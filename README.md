# worst-custom-executor-ever

## What is it?

The name says it all..

This is a companion project to a training presentation series that aims to help Support Engineers learn more about
the GitLab Runner and how it works.

We could study the existing GitLab Runner code and the existing executors, but sometimes it's easier to learn in a
practical way. By implementing the minimal functionality for a custom executor, this will provide a greater understanding
to how existing executors work at a conceptual level.

## Can I use it?

I wouldn't advise it, but you do you.

## How can I use it?
1. Install the GitLab Runner and Git on your Runner host, as these are [pre-requisites for a custom executor](https://docs.gitlab.com/runner/executors/custom.html#prerequisite-software-for-running-a-job).
2. Clone this repository onto your Runner host into `/opt`.
3. Create a new Instance/Group/Project Runner
4. Register the Runner with the "custom" executor, then add the following to
`/etc/gitlab-runner/config.toml` to the `[[runners]]` entry for the custom executor runner:

```
  [runners.custom]
    config_exec = "/opt/worst-custom-executor-ever/config.sh"
    prepare_exec = "/opt/worst-custom-executor-ever/prepare.sh"
    run_exec = "/opt/worst-custom-executor-ever/run.sh"
    cleanup_exec = "/opt/worst-custom-executor-ever/cleanup.sh"
```
